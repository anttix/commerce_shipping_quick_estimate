<?php
/**
 * Commerce_shipping_quick_estimate.
 *
 * Provides functionality to allow for
 * shipping estimates to be quickly returned to user.
 */

/**
 * Implements hook_block_info().
 */
function commerce_shipping_quick_estimate_block_info() {
  $blocks['shipping'] = array(
    'info' => t('Shipping Estimate Calculator'),
    'status' => FALSE,
    'weight' => 0,
    'visibility' => 1,
    'region' => 'content',
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function commerce_shipping_quick_estimate_block_view($delta = '') {
  switch ($delta) {
    case 'shipping':
      $block['subject'] = 'Shipping Estimate Calculator';
      $block['content'] = drupal_get_form('commerce_shipping_quick_estimate_form_shipping_estimate');
      return $block;
    break;
  }
}

/**
 * Implements hook_form().
 */
function commerce_shipping_quick_estimate_form_shipping_estimate($form, &$form_state) {
  $form['state'] = array(
    '#title' => "State",
    '#type' => 'select',
    '#options' => array(
      '0' => t('Please Select'),
      'AL' => t('Alabama'),
      'AK' => t('Alaska'),
      'AZ' => t('Arizona'),
      'AR' => t('Arkansas'),
      'CA' => t('California'),
      'CO' => t('Colorado'),
      'CT' => t('Connecticut'),
      'DE' => t('Delaware'),
      'DC' => t('District of Columbia'),
      'FL' => t('Florida'),
      'GA' => t('Georgia'),
      'HI' => t('Hawaii'),
      'ID' => t('Idaho'),
      'IL' => t('Illinois'),
      'IN' => t('Indiana'),
      'IA' => t('Iowa'),
      'KS' => t('Kansas'),
      'KY' => t('Kentucky'),
      'LA' => t('Louisiana'),
      'ME' => t('Maine'),
      'MD' => t('Maryland'),
      'MA' => t('Massachusetts'),
      'MI' => t('Michigan'),
      'MN' => t('Minnesota'),
      'MS' => t('Mississippi'),
      'MO' => t('Missouri'),
      'MY' => t('Montana'),
      'NE' => t('Nebraska'),
      'NV' => t('Nevada'),
      'NH' => t('New Hampshire'),
      'NJ' => t('New Jersey'),
      'NM' => t('New Mexico'),
      'NY' => t('New York'),
      'NC' => t('North Carolina'),
      'ND' => t('North Dakota'),
      'OH' => t('Ohio'),
      'OK' => t('Oklahoma'),
      'OR' => t('Oregon'),
      'PA' => t('Pennsylvania'),
      'RI' => t('Rhode Island'),
      'SC' => t('South Carolina'),
      'SD' => t('South Dakota'),
      'TN' => t('Tennessee'),
      'TX' => t('Texas'),
      'UT' => t('Utah'),
      'VT' => t('Vermont'),
      'VA' => t('Virginia'),
      'WA' => t('Washington'),
      'WV' => t('West Virginia'),
      'WI' => t('Wisconsin'),
      'WY' => t('Wyoming'),
    ),
  );
  $form['postal_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#ajax' => array(
      'callback' => 'commerce_shipping_quick_estimate_callback',
      'wrapper' => 'box',
    ),
    '#value' => t('Submit'),
  );
  $form['box'] = array(
    '#type' => 'markup',
    '#prefix' => '<div id="box">',
    '#suffix' => '</div>',
    '#markup' => '',
  );
  return $form;
}

/**
 * Implements hook_callback().
 *
 * Callback to dynamically pull in submitted form data and output necessary
 * estimate data to user.
 */
function commerce_shipping_quick_estimate_callback($form, $form_state) {
  // Validation.
  $element = $form['box'];
  $errors = FALSE;

  if ($form_state['values']['state'] == "0") {
    $output = "State is required.  ";
    $errors = TRUE;
  }
  if ($form_state['values']['postal_code'] == NULL) {
    $output .= "Postal code is required.";
    $errors = TRUE;
  }

  // Output.
  if ($errors == TRUE) {
    $element['#markup'] = $output;
    return $element;
  }
  else {
    // Grab User Object.
    global $user;

    // Load User Cart and set necessary profile information.
    $order = commerce_cart_order_load($user->uid);

    $profile_shipping = commerce_customer_profile_new('shipping', $user->uid);
    $profile_billing = commerce_customer_profile_new('billing', $user->uid);

    $profile_shipping->commerce_customer_address = array(
      LANGUAGE_NONE => array(
        0 => array(
          'country' => "US",
          'administrative_area' => $form_state['values']['state'],
          'postal_code' => $form_state['values']['postal_code'],
        ),
      ));

    $profile_billing->commerce_customer_address = array(
      LANGUAGE_NONE => array(
        0 => array(
          'country' => "US",
          'administrative_area' => $form_state['values']['state'],
          'postal_code' => $form_state['values']['postal_code'],
        ),
      ));
    commerce_customer_profile_save($profile_billing);
    commerce_customer_profile_save($profile_shipping);

    $order->commerce_customer_shipping[LANGUAGE_NONE][0]['profile_id'] = $profile_shipping->profile_id;
    $order->commerce_customer_billing[LANGUAGE_NONE][0]['profile_id'] = $profile_billing->profile_id;

    // Collect shipping info and output as a list or error message.
    commerce_shipping_collect_rates($order);
    $shipping_options = commerce_shipping_service_rate_options($order);

    $output = NULL;

    if (!empty($shipping_options)) {
      $output = "<ul>";
      foreach ($shipping_options as $option) {
        $output .= "<li>" . $option . "</li>";
      }
      $output .= "</ul>";
    }
    else {
      $output = "The information provided did not return any estimates.  <br />  Please verify your state and postal code.";
    }
    $output_header = "<h3>Your Estimate:</h3>";

    $element['#markup'] = $output_header . $output;
    return $element;
  }
}
